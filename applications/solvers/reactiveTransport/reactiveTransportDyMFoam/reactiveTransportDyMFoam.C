/*---------------------------------------------------------------------------*\

License
    This file is part of GeoChemFoam, an Open source software using OpenFOAM
    for multiphase multicomponent reactive transport simulation in pore-scale
    geological domain.

    GeoChemFoam is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version. See <http://www.gnu.org/licenses/>.

    The code was developed by Dr Julien Maes as part of his research work for
    the Carbonate Reservoir Group at Heriot-Watt University. Please visit our
    website for more information <https://carbonates.hw.ac.uk>.

Application
    reactiveTransportDyMFoam

Description
    Solves reactive transport equation with ALE mesh for multi-species flow

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "simpleControl.H"
#include "speciesTable.H"
#include "reactionModule.H"
#include "phreeqcModule.H"
#include "singlePhaseTransportModel.H"
#include "turbulenceModel.H"
#include "dynamicFvMesh.H"
#include "pimpleControl.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"

#   include "createTime.H"
#   include "createDynamicFvMesh.H"

    simpleControl simple(mesh);
    pimpleControl pimple(mesh);

#   include "initContinuityErrs.H"
#   include "createFields.H"

	Info << "Create Reaction Module" << endl;
	reactionModule* rm = new phreeqcModule(solutionSpecies,surfaceSpecies,surfaceMasters,selectedOutputNames,mesh,Y,sY,sOut,alpha1,I,Surf,psi);

#   include "createControls.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "\nStarting time loop\n" << endl;

    while (runTime.run())
    {
#       include "readControls.H"
#       include "CourantNo.H"
#       include "setDeltaT.H"

        // Make the fluxes absolute
        fvc::makeAbsolute(phi, U);

        runTime++;

        Info<< "Time = " << runTime.timeName() << nl << endl;

        mesh.update();

#       include "volContinuity.H"

        if (checkMeshCourantNo)
        {
#           include "meshCourantNo.H"
        }

        // Mesh motion update
        if (correctPhi && mesh.changing())
        {
            // Fluxes will be corrected to absolute velocity
            // HJ, 6/Feb/2009
#           include "correctPhi.H"
        }

        if (mesh.changing())
        {
#           include "CourantNo.H"
        }

        // Make the fluxes relative to the mesh motion
        fvc::makeRelative(phi, U);


		#include "YiEqn.H"

        // --- PIMPLE loop
        while (pimple.loop())
        {
#           include "UEqn.H"

            // --- PISO loop
            while (pimple.correct())
            {
#               include "pEqn.H"
            }

            turbulence->correct();
        }

        runTime.write();

        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << nl << endl;
    }

	Info << "Destroy Reaction Module" << endl;
	delete rm;

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
