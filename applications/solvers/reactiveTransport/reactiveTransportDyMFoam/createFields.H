    //alpha1=1.0 is set for the reaction module
    volScalarField alpha1
    (
        IOobject
        (
            "alpha1",
            runTime.timeName(),
            mesh
        ),
		mesh,
        scalar(1.0)
    );    Info<< "Reading field p\n" << endl;
    volScalarField p
    (
        IOobject
        (
            "p",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    Info<< "Reading field U\n" << endl;

    volVectorField U
    (
        IOobject
        (
            "U",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

    Info<< "Reading transportProperties\n" << endl;

    IOdictionary transportProperties
    (
        IOobject
        (
            "transportProperties",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

#   include "createPhi.H"

#   include "createY.H"

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

    Info<< "Reading field Surf\n" << endl;
    volScalarField Surf
    (
        IOobject
        (
            "Surf",
            runTime.timeName(),
            mesh,
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE
        ),
        mesh
    );

	
	wordList selectedOutputNames;
	if (transportProperties.found("selectedOutput"))
	{
		Info << "Reading selected output" << endl;
		const dictionary& selectedOutputDict = transportProperties.subDict("selectedOutput");
		Info << "Reading solution species name\n" << endl;
	    selectedOutputNames = selectedOutputDict.toc();
	}


	Info << "Create selected output vector\n" << endl;
	PtrList<volScalarField> sOut(selectedOutputNames.size());

	forAll(selectedOutputNames, i)
	{
		sOut.set
		(
			i,
			new volScalarField
			(
				IOobject
				(
					selectedOutputNames[i],
					mesh.time().timeName(),
					mesh,
					IOobject::MUST_READ,
					IOobject::AUTO_WRITE
				),
				mesh
			)
		);
	}

	//surface potential
    volScalarField psi
    (
        IOobject
        (
            "psi",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
		dimensionedScalar("psi", dimensionSet(1,2,-3,0,0,-1,0), 0.0)
    );

	//ionic strength
    volScalarField I
    (
        IOobject
        (
            "I",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
		dimensionedScalar("I", dimMoles/dimVolume, 0.0)
    );


    label pRefCell = 0;
    scalar pRefValue = 0.0;
    setRefCell(p, pimple.dict(), pRefCell, pRefValue);
    mesh.schemesDict().setFluxRequired(p.name());

    singlePhaseTransportModel laminarTransport(U, phi);

    autoPtr<incompressible::turbulenceModel> turbulence
    (
        incompressible::turbulenceModel::New(U, phi, laminarTransport)
    );

    Info<< "Reading field aU if present\n" << endl;
    volScalarField aU
    (
        IOobject
        (
            "aU",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        1/runTime.deltaT(),
        zeroGradientFvPatchScalarField::typeName
    );
