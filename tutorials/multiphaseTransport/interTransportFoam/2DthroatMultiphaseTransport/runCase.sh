#!/bin/bash

set -e

cp 0/alpha1.org 0/alpha1
cp 0/T.org 0/T
blockMesh
setFields
decomposePar
mpiexec -np 4 interTransportFoam -parallel
reconstructPar
rm -rf process*
