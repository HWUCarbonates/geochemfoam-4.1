#!/bin/bash

set -e

source $HOME/.bashrc
of4x

rm -f constant/cell*
rm -f constant/point*
rm -f constant/polyMesh/cell*
rm -f constant/polyMesh/point*
rm -f constant/polyMesh/face*
cp system/controlDictInit system/controlDict
blockMesh
cp system/snappyHexMeshDict1 system/snappyHexMeshDict
snappyHexMesh -overwrite
cp system/snappyHexMeshDict2 system/snappyHexMeshDict
snappyHexMesh -overwrite
checkMesh
createPatch -overwrite
transformPoints -scale '(0.01 0.01 0.01)'

compactFaceToFace
