#!/bin/bash

set -e

cp constant/transportPropertiesH10 constant/transportProperties

cp system/controlDictRun system/controlDict
decomposePar
mpiexec -np 4 interTransportFoam -parallel
reconstructPar
rm -rf processor*
