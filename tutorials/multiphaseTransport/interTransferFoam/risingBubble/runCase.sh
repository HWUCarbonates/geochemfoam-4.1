#!/bin/bash


set -e


decomposePar
mpiexec -np 24 interTransferFoam -parallel
reconstructPar
rm -rf processor*
