#!/bin/bash

set -e

blockMesh
cp 0/alpha1.org 0/alpha1
cp 0/T.org 0/T
setFields
decomposePar
mpiexec -np 8 interTransferFoam -parallel
reconstructPar
rm -rf processor*
