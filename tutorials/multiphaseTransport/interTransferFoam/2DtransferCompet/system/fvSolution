/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | foam-extend: Open Source CFD                    |
|  \\    /   O peration     | Version:     4.0                                |
|   \\  /    A nd           | Web:         http://www.foam-extend.org         |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    Yi 
    {
        solver          BICCG;
        preconditioner  DILU;
        tolerance       1e-09;
        relTol          0;
    }
    
    YiFinal
    {
        $Yi;
    }

    pcorr
    {
        solver          GAMG;
        tolerance       1e-8;
        relTol          0.00001;
        smoother        GaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration on;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    pd 
    {
        solver          GAMG;
        tolerance       1e-8;
        relTol          0.01;
        smoother        GaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration on;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    pdFinal
    {
        solver          GAMG;
        tolerance       1e-8;
        relTol          0;
        smoother        GaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration on;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    pc
    {
        solver          GAMG;
        tolerance       1e-6;
        relTol          0.1;
        smoother        GaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration on;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    pcFinal
    {
        solver          GAMG;
        tolerance       1e-7;
        relTol          0.001;
        smoother        GaussSeidel;
        nPreSweeps      0;
        nPostSweeps     2;
        nFinestSweeps   2;
        cacheAgglomeration on;
        nCellsInCoarsestLevel 10;
        agglomerator    faceAreaPair;
        mergeLevels     1;
    }

    U
    {
        solver          BiCGStab;
        preconditioner  DILU;
        tolerance       1e-06;
        relTol          0;
    }
}

PISO
{
    cAlpha 1.0;
    cYi 1.0;
    maxConcentration
    {
       T 1.0;
       A 1.0;
    }
}

PIMPLE
{
    momentumPredictor   yes;
    nOuterCorrectors    1;
    nCorrectors         3;
    nNonOrthogonalCorrectors 0;
    pRefCell            0;
    pRefValue           0;
    pdRefCell           0;
    pdRefValue          0;
    pcRefCell           0;
    pcRefValue          0;
    nAlphaCorr          1; 
    nAlphaSubCycles     1;
    cPc                 0;
    cSK                 0.5;
    nSK                 2;
    cfcFilt             0;
    cfcFiltRelax        0;
    cphicFilt           0;
    gPcCorr             no;
}
 



relaxationFactors
{
    U               0.66;
        p               0.66;
}

SIMPLE
{
    nNonOrthogonalCorrectors 0;
}

// ************************************************************************* //
