#!/bin/bash

set -e

source /usr/local/OpenFOAM/OpenFOAM-4.x/etc/bashrc

rm -f constant/cell*
rm -f constant/point*
rm -f constant/polyMesh/cell*
rm -f constant/polyMesh/point*
rm -f constant/polyMesh/face*
cp system/controlDictInit system/controlDict
blockMesh
cp system/snappyHexMeshDict1 system/snappyHexMeshDict
snappyHexMesh -overwrite
cp system/snappyHexMeshDict2 system/snappyHexMeshDict
snappyHexMesh -overwrite
checkMesh
createPatch -overwrite
transformPoints -scale '(0.01 0.01 0.01)'

compactFaceToFace
