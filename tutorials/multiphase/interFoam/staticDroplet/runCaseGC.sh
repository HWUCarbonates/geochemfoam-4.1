#!/bin/bash

set -e

blockMesh
cp 0/alpha1.org 0/alpha1
setFields
cp system/controlDict1 system/controlDict
decomposePar
mpiexec -np 4 interGCFoam -parallel
reconstructPar
rm -rf proc*
