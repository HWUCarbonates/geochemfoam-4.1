#!/bin/bash

set -e

blockMesh

cp system/controlDict2 system/controlDict
decomposePar
mpiexec -np 4 interFoam -parallel
reconstructPar
rm -rf proc*
