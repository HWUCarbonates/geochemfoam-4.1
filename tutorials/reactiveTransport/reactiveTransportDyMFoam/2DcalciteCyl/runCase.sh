#!/bin/bash

set -e

cp system/controlDictRun system/controlDict

decomposePar
mpiexec -np 2 reactiveTransportDyMFoam -parallel
reconstructPar
rm -rf processor*
