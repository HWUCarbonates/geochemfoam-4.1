#!/bin/bash

set -e

blockMesh
decomposePar
mpiexec -np 4 reactiveTransportFoam -parallel
reconstructPar
rm -rf processor*

