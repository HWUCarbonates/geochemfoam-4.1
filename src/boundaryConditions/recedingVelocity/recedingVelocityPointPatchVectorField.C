/*---------------------------------------------------------------------------*\

License
    This file is part of GeoChemFoam, an Open source software using OpenFOAM
    for multiphase multicomponent reactive transport simulation in pore-scale
    geological domain.

    GeoChemFoam is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version. See <http://www.gnu.org/licenses/>.

    The code was developed by Dr Julien Maes as part of his research work for
    the Carbonate Reservoir Group at Heriot-Watt University. Please visit our
    website for more information <https://carbonates.hw.ac.uk>.

\*---------------------------------------------------------------------------*/

#include "recedingVelocityPointPatchVectorField.H"
#include "pointPatchFields.H"
#include "addToRunTimeSelectionTable.H"
#include "fvPatchFieldMapper.H"
#include "volFields.H"
#include "foamTime.H"
#include "polyMesh.H"
#include "primitivePatchInterpolation.H"
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

recedingVelocityPointPatchVectorField::
recedingVelocityPointPatchVectorField
(
    const pointPatch& p,
    const DimensionedField<vector, pointMesh>& iF
)
:
    fixedValuePointPatchVectorField(p, iF),
    p0_(p.localPoints()),
	CName1_("C"),
	CName2_("C"),
	phaseName_("Calcite"),
	k1_(0.0),
	k2_(0.0),
	k3_(0.0),
	rhos_(0.0),
    Mw_(0.0)
{}


recedingVelocityPointPatchVectorField::
recedingVelocityPointPatchVectorField
(
    const pointPatch& p,
    const DimensionedField<vector, pointMesh>& iF,
    const dictionary& dict
)
:
    fixedValuePointPatchVectorField(p, iF, dict),
	CName1_(dict.lookup("CName1")),
	CName2_(dict.lookup("CName2")),
	phaseName_(dict.lookup("phaseName")),
    k1_(readScalar(dict.lookup("k1"))),
    k2_(readScalar(dict.lookup("k2"))),
    k3_(readScalar(dict.lookup("k3"))),
    rhos_(readScalar(dict.lookup("rhos"))),
    Mw_(readScalar(dict.lookup("Mw")))
{
    if (!dict.found("value"))
    {
        updateCoeffs();
    }

    if (dict.found("p0"))
    {
        p0_ = vectorField("p0", dict , p.size());
    }
    else
    {
        p0_ = p.localPoints();
    }
}


recedingVelocityPointPatchVectorField::
recedingVelocityPointPatchVectorField
(
    const recedingVelocityPointPatchVectorField& ppf,
    const pointPatch& p,
    const DimensionedField<vector, pointMesh>& iF,
    const PointPatchFieldMapper& mapper
)
:
    fixedValuePointPatchVectorField(ppf, p, iF, mapper),
    p0_(ppf.p0_, mapper),
    CName1_(ppf.CName1_),
    CName2_(ppf.CName2_),
    phaseName_(ppf.phaseName_),
	k1_(ppf.k1_),
	k2_(ppf.k2_),
	k3_(ppf.k3_),
	rhos_(ppf.rhos_),
	Mw_(ppf.Mw_)
{}


recedingVelocityPointPatchVectorField::
recedingVelocityPointPatchVectorField
(
    const recedingVelocityPointPatchVectorField& ppf,
    const DimensionedField<vector, pointMesh>& iF
)
:
    fixedValuePointPatchVectorField(ppf, iF),
    p0_(ppf.p0_),
    CName1_(ppf.CName1_),
    CName2_(ppf.CName2_),
    phaseName_(ppf.phaseName_),
	k1_(ppf.k1_),
	k2_(ppf.k2_),
	k3_(ppf.k3_),
	rhos_(ppf.rhos_),
	Mw_(ppf.Mw_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void recedingVelocityPointPatchVectorField::autoMap
(
    const PointPatchFieldMapper& m
)
{
    fixedValuePointPatchVectorField::autoMap(m);

    p0_.autoMap(m);
}


void recedingVelocityPointPatchVectorField::rmap
(
    const pointPatchField<vector>& ptf,
    const labelList& addr
)
{
    const recedingVelocityPointPatchVectorField& oVptf =
        refCast<const recedingVelocityPointPatchVectorField>(ptf);

    fixedValuePointPatchVectorField::rmap(oVptf, addr);

    p0_.rmap(oVptf.p0_, addr);
}


void recedingVelocityPointPatchVectorField::updateCoeffs()
{
    if (this->updated())
    {
        return;
    }

    const polyMesh& mesh = patch().boundaryMesh().mesh()();
    const Time& t = mesh.time();
    const pointPatch& p = this->patch();

	const volScalarField& C1 = this->db().objectRegistry::lookupObject<volScalarField>(CName1_);
	const volScalarField& C2 = this->db().objectRegistry::lookupObject<volScalarField>(CName2_);

	const volScalarField& si = this->db().objectRegistry::lookupObject<volScalarField>("si_"+phaseName_);

	const fvPatchField<scalar>& C1p = C1.boundaryField()[patch().index()];
	const fvPatchField<scalar>& C2p = C2.boundaryField()[patch().index()];
	const fvPatchField<scalar>& sip = si.boundaryField()[patch().index()];

	vectorField nd = patch().pointNormals();

	//- set-up interpolator
	primitivePatchInterpolation patchInterpolator
	(
	     mesh.boundaryMesh()[patch().index()]
    );

	scalarField pointValues1 =
    patchInterpolator.faceToPointInterpolate<scalar>
    (
        C1p
    );

	scalarField pointValues2 =
    patchInterpolator.faceToPointInterpolate<scalar>
    (
        C2p
    );

	scalarField siPointValues =
    patchInterpolator.faceToPointInterpolate<scalar>
    (
        sip
    );

    Field<vector>::operator=
    (
        0.0*(p0_ - p.localPoints())/t.deltaTValue() + nd*(k1_*max(pointValues1,0.0)+k2_*max(pointValues2,0.0)+k3_)*(1-pow(10,siPointValues))*Mw_/rhos_
    );

    fixedValuePointPatchVectorField::updateCoeffs();
}


void recedingVelocityPointPatchVectorField::write(Ostream& os) const
{
    pointPatchField<vector>::write(os);
    os.writeKeyword("CName1")
        << CName1_ << token::END_STATEMENT << nl;
    os.writeKeyword("CName2")
        << CName2_ << token::END_STATEMENT << nl;
    os.writeKeyword("phaseName")
        << phaseName_ << token::END_STATEMENT << nl;
    os.writeKeyword("k1")
        << k1_ << token::END_STATEMENT << nl;
    os.writeKeyword("k2")
        << k2_ << token::END_STATEMENT << nl;
    os.writeKeyword("k3")
        << k3_ << token::END_STATEMENT << nl;
    os.writeKeyword("rhos")
        << rhos_ << token::END_STATEMENT << nl;
    os.writeKeyword("Mw")
        << Mw_ << token::END_STATEMENT << nl;
    writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makePointPatchTypeField
(
    pointPatchVectorField,
    recedingVelocityPointPatchVectorField
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
