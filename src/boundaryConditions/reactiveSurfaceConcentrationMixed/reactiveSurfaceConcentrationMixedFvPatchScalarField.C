/*---------------------------------------------------------------------------*\

License
    This file is part of GeoChemFoam, an Open source software using OpenFOAM
    for multiphase multicomponent reactive transport simulation in pore-scale
    geological domain.

    GeoChemFoam is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version. See <http://www.gnu.org/licenses/>.

    The code was developed by Dr Julien Maes as part of his research work for
    the Carbonate Reservoir Group at Heriot-Watt University. Please visit our
    website for more information <https://carbonates.hw.ac.uk>.

\*---------------------------------------------------------------------------*/

#include "reactiveSurfaceConcentrationMixedFvPatchScalarField.H"
#include "addToRunTimeSelectionTable.H"
#include "fvPatchFieldMapper.H"
#include "volFields.H"
#include "surfaceFields.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::
reactiveSurfaceConcentrationMixedFvPatchScalarField
(
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF
)
:
    mixedFvPatchScalarField(p, iF),
    k_(0.0),
	phaseName_("Calcite")
{
    this->refValue() = pTraits<scalar>::zero;
    this->refGrad() = pTraits<scalar>::zero;
    this->valueFraction() = 0.0;
}

Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::
reactiveSurfaceConcentrationMixedFvPatchScalarField
(
    const reactiveSurfaceConcentrationMixedFvPatchScalarField& ptf,
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    mixedFvPatchScalarField(ptf, p, iF, mapper),
    k_(ptf.k_),
    phaseName_(ptf.phaseName_)
{}

Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::
reactiveSurfaceConcentrationMixedFvPatchScalarField
(
    const fvPatch& p,
    const DimensionedField<scalar, volMesh>& iF,
    const dictionary& dict
)
:
    mixedFvPatchScalarField(p, iF),
    k_(readScalar(dict.lookup("k"))),
	phaseName_(dict.lookup("phaseName"))
{
    this->refValue() = pTraits<scalar>::zero;
    if (dict.found("value"))
    {
        fvPatchField<scalar>::operator=
        (
            scalarField("value", dict, p.size())
        );
    }
    else
    {
        fvPatchField<scalar>::operator=(this->refValue());
    }

    this->refGrad() = pTraits<scalar>::zero;
    this->valueFraction() = 0.0;
}




Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::
reactiveSurfaceConcentrationMixedFvPatchScalarField
(
    const reactiveSurfaceConcentrationMixedFvPatchScalarField& tppsf
)
:
    mixedFvPatchScalarField(tppsf),
    k_(tppsf.k_),
    phaseName_(tppsf.phaseName_)
{}


Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::
reactiveSurfaceConcentrationMixedFvPatchScalarField
(
    const reactiveSurfaceConcentrationMixedFvPatchScalarField& tppsf,
    const DimensionedField<scalar, volMesh>& iF
)
:
    mixedFvPatchScalarField(tppsf, iF),
    k_(tppsf.k_),
    phaseName_(tppsf.phaseName_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::updateCoeffs()
{
    if (updated())
    {
        return;
    }
	const dictionary& transportProperties = db().lookupObject<IOdictionary> ("transportProperties");

	const dictionary& solutionSpeciesDict = transportProperties.subDict("solutionSpecies");

	const dictionary& subdict = solutionSpeciesDict.subDict(this->dimensionedInternalField().name());

	dimensionedScalar D(subdict.lookup("D"));

	const volScalarField& si = this->db().objectRegistry::lookupObject<volScalarField>("si_"+phaseName_);
	const fvPatchField<scalar>& sip = patch().patchField<volScalarField, scalar>(si);

	scalarField lambda = k_*(1-pow(10,sip))/D.value()/(this->patch().deltaCoeffs());

    valueFraction() = lambda/(lambda + 1);

    mixedFvPatchScalarField::updateCoeffs();
}


void Foam::reactiveSurfaceConcentrationMixedFvPatchScalarField::write(Ostream& os) const
{
    os.writeKeyword("k") << k_ << token::END_STATEMENT << nl;
    os.writeKeyword("phaseName")
        << phaseName_ << token::END_STATEMENT << nl;
	mixedFvPatchScalarField::write(os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    makePatchTypeField
    (
        fvPatchScalarField,
        reactiveSurfaceConcentrationMixedFvPatchScalarField
    );
}

// ************************************************************************* //
